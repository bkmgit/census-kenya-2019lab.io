function make_map(cityname, dom_id){
  achart = echarts.init(document.getElementById(dom_id));
  var option =  {
    "title": [
      {
	"textStyle": {
	  "color": "#000",
	  "fontSize": 18
	},
	"subtext": "",
	"text": cityname,
	"top": "auto",
	"subtextStyle": {
	  "color": "#aaa",
	  "fontSize": 12
	},
	"left": "auto"
      }
    ],
    "legend": [
      {
	"selectedMode": "multiple",
	"top": "top",
	"orient": "horizontal",
	"data": [
	  ""
	],
	"left": "center",
	"show": true
      }
    ],
            visualMap: {
            left: 'right',
            min:  0,
            max: 25,
            inRange: {
                color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026']
            },
            text: ['High', 'Low'],          
            calculable: true
        },

    "backgroundColor": "#fff",
    	  tooltip: {
		  trigger: 'item',
		  formatter: function (params) {
			  var value = (params.value + '').split('.');
			  value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
			  return params.seriesName + '<br/>' + params.name + ' : ' + value;
		  }
	  },
	  toolbox: {
		  show: true,
		  orient: 'vertical',
		  left: 'right',
		  top: 'center',
        feature: {
            dataView: {readOnly: true},
            restore: {},
            saveAsImage: {}
        }
    },
    "series": [
      {
	"mapType": cityname,
	"data": [
           {name: "Mombasa", value: 13},
           {name: "Kwale", value: 4.3},
           {name: "Kilifi", value: 5.5},
           {name: "Tana River", value: 2.7},
           {name: "Lamu", value: 4.2},
           {name: "Taita Taveta", value: 6.3},
           {name: "Garissa", value: 2.6},
           {name: "Wajir", value: 1.4},
           {name: "Mandera", value: 1.5},
           {name: "Marsabit", value: 2.5},
           {name: "Isiolo", value: 4.5},
           {name: "Meru", value: 4.3},
           {name: "Tharaka-Nithi", value: 4.9},
           {name: "Embu", value: 6.3},
           {name: "Kitui", value: 3.5},
           {name: "Machakos", value: 9.4},
           {name: "Makueni", value: 4.0},
           {name: "Nyandarua", value: 4.6},
           {name: "Nyeri", value: 8.6},
           {name: "Kirinyaga", value: 5.9},
           {name: "Murang`a", value: 4.9},
           {name: "Kiambu", value: 19.6},
           {name: "Turkana", value: 2.1},
           {name: "West Pokot", value: 1.4},
           {name: "Samburu", value: 3.2},
           {name: "Trans Nzoia", value: 4.9},
           {name: "Uasin Gishu", value: 10.1},
           {name: "Elegeyo-Marakwet", value: 2.0},
           {name: "Nandi", value: 3.6},
           {name: "Baringo", value: 3.2},
           {name: "Laikipia", value: 6.9},
           {name: "Nakuru", value: 9.6},
           {name: "Narok", value: 2.7},
           {name: "Kajiado", value: 14.6},
           {name: "Kericho", value: 4.7},
           {name: "Bomet", value: 3.3},
           {name: "Kakamega", value: 4.3},
           {name: "Vihiga", value: 4.3},
           {name: "Bungoma", value: 4.1},
           {name: "Busia", value: 3.7},
           {name: "Siaya", value: 4.2},
           {name: "Kisumu", value: 9.8},
           {name: "Homa Bay", value: 4.1},
           {name: "Migori", value: 4.2},
           {name: "Kisii", value: 5.0},
           {name: "Nyamira", value: 3.7},
           {name: "Nairobi", value: 22.7}
    ],
	"name": "Kenya Proportion of Households with Computers/Tablets (2019)",
	"type": "map",
	"roam": true,
    emphasis: {
				  label: {
					  show:true
				  }
			  }
      },
      
    ]
  };
  achart.setOption(option);
}
